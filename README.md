# LibreTimeBase: An OSHW Controller & Distribution Amplifier for Rubidium Frequency Standards #
check out the [dev branch](https://bitbucket.org/christandlg/libretimebase/src/dev/)!

The goal of this project is to create a device that can interface with a wide range of Rubidium Frequency Standards and provide additional functionality:

 - Distribution Amplifier with isolated outputs
 - Clock Generation and 5V TTL clock output
 - Calibration and adjustments via Electronic Frequency Control or Serial Interface
 - 1PPS in as a calibration reference or master clock for the Rubidium Frequency Standard
 - 1PPS output with user selectable pulse length (and frequency, need not be 1PPS)
 - (optional) built-in GNSS receiver for calibration
 - Monitoring of operating parameters and health indicators (Built In Test Equipment, Supply Current, Temperature, Lamp Voltages, ...)
 - Supply Voltage monitoring, overvoltage and reverse voltage protection
 
The firmware can be found [here](https://bitbucket.org/christandlg/libretimebase-firmware/).

Image (WIP):

![LibreTimeBase WIP Render](LibreTimeBase_AR133.png)

The device is designed to be used either as a module for the [Envox EEZ BB3](https://www.envox.eu/eez-bench-box-3/) or as a standalone device. 

When used as a BB3 module, functionality will be exposed via the BB3 User Interface and SCPI via the BB3's remote interfaces. 

When used as a standalone device a SCPI interface will be exposed via USB. Unfortunately, with most types of Rubidium Frequency Standard installed the device will take up more than 1 slot of the BB3. The PCB is 100mm wide, so the device can be housed in common extruded aluminum cases for 100mm PCBs. 
