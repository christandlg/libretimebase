EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_Coaxial J?
U 1 1 61C41C18
P 3100 3450
AR Path="/61C41C18" Ref="J?"  Part="1" 
AR Path="/626B0C12/61C41C18" Ref="J?"  Part="1" 
AR Path="/614B3CDA/61C41C18" Ref="J?"  Part="1" 
AR Path="/60F7742E/61AE0BD9/61C41C18" Ref="J?"  Part="1" 
AR Path="/60F7742E/61ADFCBD/61C41C18" Ref="J?"  Part="1" 
AR Path="/60F7742E/61AE0437/61C41C18" Ref="J1401"  Part="1" 
F 0 "J1401" H 3200 3425 50  0000 L CNN
F 1 "Amphenol 132289" H 3200 3334 50  0000 L CNN
F 2 "Connector_Coaxial:SMA_Amphenol_132291-12_Vertical" H 3100 3450 50  0001 C CNN
F 3 " ~" H 3100 3450 50  0001 C CNN
F 4 "NO" H 3100 3450 50  0001 C CNN "Place"
F 5 "NO" H 3100 3450 50  0001 C CNN "Provided"
F 6 "523-132289" H 3100 3450 50  0001 C CNN "Mouser"
F 7 "132289" H 3100 3450 50  0001 C CNN "TME"
	1    3100 3450
	-1   0    0    -1  
$EndComp
NoConn ~ 4000 6350
NoConn ~ 4000 6150
NoConn ~ 4000 6250
NoConn ~ 4500 5350
Wire Wire Line
	4000 5550 3200 5550
Wire Wire Line
	4000 5650 3200 5650
NoConn ~ 4000 5850
Wire Wire Line
	3950 6850 3200 6850
Text Label 5150 4950 2    50   ~ 0
AR60A_+15V
Text Notes 10000 5900 0    50   ~ 0
NC
Text Notes 9550 5600 0    50   ~ 0
RFU
Text Notes 9700 5000 2    50   ~ 0
NC
Text Notes 9700 5700 2    50   ~ 0
GND
Text Notes 10000 6000 0    50   ~ 0
GND
Text Notes 9700 5300 2    50   ~ 0
uC_UART_Rx_RS232
Text Notes 9700 5200 2    50   ~ 0
uC_UART_Tx_RS232
Text Notes 10000 5300 0    50   ~ 0
NC
Text Notes 10000 5100 0    50   ~ 0
NC
Text Notes 10000 6200 0    50   ~ 0
GND
Text Notes 9700 4700 2    50   ~ 0
GND
Text Notes 9700 4600 2    50   ~ 0
GND
Text Notes 9700 4900 2    50   ~ 0
GND
Text Notes 10000 5800 0    50   ~ 0
GND
Text Notes 10000 5700 0    50   ~ 0
GND
Text Notes 10000 5200 0    50   ~ 0
GND
Text Notes 9700 6500 2    50   ~ 0
VDD1
Text Notes 9700 6400 2    50   ~ 0
VDD1
Text Notes 10000 4800 0    50   ~ 0
VDD0
Text Notes 10000 4700 0    50   ~ 0
VDD0
Text Notes 10000 6400 0    50   ~ 0
GND
Text Notes 9700 6200 2    50   ~ 0
NC
Text Notes 10000 5400 0    50   ~ 0
GND
Text Notes 9700 5100 2    50   ~ 0
GND
Text Notes 10000 4600 0    50   ~ 0
VDD0
Wire Notes Line
	9950 6500 9950 4550
Wire Notes Line
	9750 6500 9750 4550
Wire Notes Line
	9950 4550 9750 4550
Wire Notes Line
	9950 6500 9750 6500
Text Notes 10000 6100 0    50   ~ 0
RFS_OUT
Text Notes 9700 6100 2    50   ~ 0
NC
Text Notes 10000 5000 0    50   ~ 0
1PPS_OUT
Text Notes 10000 4900 0    50   ~ 0
LOCK_INDICATOR
Text Notes 9700 5400 2    50   ~ 0
GND
Text Notes 9700 5900 2    50   ~ 0
uC_UART_Rx
Text Notes 10000 5600 0    50   ~ 0
VREF_1
Text Notes 9700 6300 2    50   ~ 0
VDD1
Text Notes 9700 5500 2    50   ~ 0
VREF_0
Text Notes 9700 5800 2    50   ~ 0
uC_UART_Tx
Text Notes 10000 6500 0    50   ~ 0
GND
Text Notes 9700 6000 2    50   ~ 0
1PPS_IN
Text Notes 10000 5500 0    50   ~ 0
EFC_IN
Text Notes 10000 6300 0    50   ~ 0
EARTH
Text Notes 9700 4800 2    50   ~ 0
EARTH
Text Notes 10350 6500 0    50   ~ 0
RFS_OUT max 10dBm
NoConn ~ 4000 5950
Connection ~ 4550 6550
Wire Wire Line
	4500 6550 4550 6550
Wire Wire Line
	4500 6650 4700 6650
Wire Wire Line
	4000 5250 3950 5250
Wire Wire Line
	4000 5150 3800 5150
Connection ~ 3950 5250
Wire Wire Line
	3950 5450 3950 5250
Text Notes 4500 7000 2    50   ~ 0
reverse protection
NoConn ~ 4500 5450
NoConn ~ 4000 5350
Wire Wire Line
	4000 5450 3950 5450
Wire Wire Line
	4550 6550 4550 6350
Connection ~ 4550 6350
Wire Wire Line
	4550 6350 4500 6350
NoConn ~ 4000 6450
NoConn ~ 4000 6550
Connection ~ 3950 5450
Wire Wire Line
	3950 5750 3950 5450
Wire Wire Line
	4000 5750 3950 5750
Connection ~ 3950 5750
Wire Wire Line
	3950 6050 3950 5750
Wire Wire Line
	4000 6050 3950 6050
Connection ~ 4550 6050
Wire Wire Line
	4500 6050 4550 6050
Wire Wire Line
	4550 6050 4550 5750
Wire Wire Line
	4550 5750 4550 5550
Connection ~ 4550 5750
Wire Wire Line
	4500 5750 4550 5750
Wire Wire Line
	4550 6150 4550 6350
Wire Wire Line
	4550 6150 4550 6050
Connection ~ 4550 6150
Wire Wire Line
	4500 6150 4550 6150
Wire Wire Line
	4550 5550 4500 5550
NoConn ~ 4500 5650
NoConn ~ 4500 6250
Wire Wire Line
	3800 4750 3800 5150
$Comp
L power:Earth #PWR?
U 1 1 62890F2B
P 3800 4750
AR Path="/62890F2B" Ref="#PWR?"  Part="1" 
AR Path="/626B0C12/62890F2B" Ref="#PWR?"  Part="1" 
AR Path="/60F7742E/61AE0437/62890F2B" Ref="#PWR0141"  Part="1" 
F 0 "#PWR0141" H 3800 4500 50  0001 C CNN
F 1 "Earth" H 3800 4600 50  0001 C CNN
F 2 "" H 3800 4750 50  0001 C CNN
F 3 "~" H 3800 4750 50  0001 C CNN
	1    3800 4750
	1    0    0    1   
$EndComp
Wire Wire Line
	4700 7050 4700 6650
Wire Wire Line
	4550 7050 4550 6850
Connection ~ 4550 6850
Wire Wire Line
	4500 6850 4550 6850
Wire Wire Line
	4550 6750 4550 6550
Wire Wire Line
	4550 6750 4550 6850
Connection ~ 4550 6750
Wire Wire Line
	4500 6750 4550 6750
$Comp
L power:GND #PWR?
U 1 1 62890F1F
P 3950 4750
AR Path="/62890F1F" Ref="#PWR?"  Part="1" 
AR Path="/626B0C12/62890F1F" Ref="#PWR?"  Part="1" 
AR Path="/60F7742E/61AE0437/62890F1F" Ref="#PWR0140"  Part="1" 
F 0 "#PWR0140" H 3950 4500 50  0001 C CNN
F 1 "GND" H 3955 4577 50  0000 C CNN
F 2 "" H 3950 4750 50  0001 C CNN
F 3 "" H 3950 4750 50  0001 C CNN
	1    3950 4750
	1    0    0    1   
$EndComp
Wire Wire Line
	3950 5250 3950 5050
Connection ~ 3950 5050
Wire Wire Line
	4000 5050 3950 5050
Wire Wire Line
	3950 5050 3950 4950
Wire Wire Line
	3950 4950 3950 4750
Connection ~ 3950 4950
Wire Wire Line
	4000 4950 3950 4950
$Comp
L power:Earth #PWR?
U 1 1 62890F07
P 4700 7050
AR Path="/62890F07" Ref="#PWR?"  Part="1" 
AR Path="/626B0C12/62890F07" Ref="#PWR?"  Part="1" 
AR Path="/60F7742E/61AE0437/62890F07" Ref="#PWR0136"  Part="1" 
F 0 "#PWR0136" H 4700 6800 50  0001 C CNN
F 1 "Earth" H 4700 6900 50  0001 C CNN
F 2 "" H 4700 7050 50  0001 C CNN
F 3 "~" H 4700 7050 50  0001 C CNN
	1    4700 7050
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62890F00
P 4550 7050
AR Path="/62890F00" Ref="#PWR?"  Part="1" 
AR Path="/626B0C12/62890F00" Ref="#PWR?"  Part="1" 
AR Path="/60F7742E/61AE0437/62890F00" Ref="#PWR0135"  Part="1" 
F 0 "#PWR0135" H 4550 6800 50  0001 C CNN
F 1 "GND" H 4555 6877 50  0000 C CNN
F 2 "" H 4550 7050 50  0001 C CNN
F 3 "" H 4550 7050 50  0001 C CNN
	1    4550 7050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4000 6650 3950 6650
Wire Wire Line
	3950 6650 3950 6750
Wire Wire Line
	3950 6750 3950 6850
Connection ~ 3950 6750
Wire Wire Line
	4000 6750 3950 6750
Connection ~ 3950 6850
Wire Wire Line
	4000 6850 3950 6850
Wire Wire Line
	4500 5150 4550 5150
Wire Wire Line
	5150 4950 4550 4950
Wire Wire Line
	4550 4950 4500 4950
Connection ~ 4550 4950
Wire Wire Line
	4550 5150 4550 5050
Wire Wire Line
	4550 5050 4550 4950
Connection ~ 4550 5050
Wire Wire Line
	4500 5050 4550 5050
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J1402
U 1 1 62890EE8
P 4200 5850
F 0 "J1402" H 4250 6967 50  0000 C CNN
F 1 "MEC8-120-02-L-DV-A" H 4250 6876 50  0000 C CNN
F 2 "LibreTimeBase:MEC8-120-02" H 4200 5850 50  0001 C CNN
F 3 "https://suddendocs.samtec.com/prints/mec8-1xx-xx-xx-dv-xx-x-xx-mkt.pdf" H 4200 5850 50  0001 C CNN
F 4 "NO" H 4200 5850 50  0001 C CNN "Place"
F 5 "NO" H 4200 5850 50  0001 C CNN "Provided"
	1    4200 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 3850 4250 3900
$Comp
L power:GND #PWR?
U 1 1 61B3D6DA
P 4250 3900
AR Path="/61B3D6DA" Ref="#PWR?"  Part="1" 
AR Path="/60F7742E/61B3D6DA" Ref="#PWR?"  Part="1" 
AR Path="/60F7742E/61AE0437/61B3D6DA" Ref="#PWR01405"  Part="1" 
F 0 "#PWR01405" H 4250 3650 50  0001 C CNN
F 1 "GND" H 4255 3727 50  0000 C CNN
F 2 "" H 4250 3900 50  0001 C CNN
F 3 "" H 4250 3900 50  0001 C CNN
	1    4250 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3450 4650 3450
Text Label 5300 3450 2    50   ~ 0
AR60A_OUT_ATT
$Comp
L RF:PAT1220-C-6DB U?
U 1 1 61B3D6D2
P 4250 3550
AR Path="/61B3D6D2" Ref="U?"  Part="1" 
AR Path="/60F7742E/61B3D6D2" Ref="U?"  Part="1" 
AR Path="/60F7742E/61AE0437/61B3D6D2" Ref="U1402"  Part="1" 
F 0 "U1402" H 4250 3917 50  0000 C CNN
F 1 "PAT1220-C-6DB" H 4250 3826 50  0000 C CNN
F 2 "LibreTimeBase:RF_Attenuator_Susumu_PAT1220" H 4250 3550 50  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition16_en.pdf" H 4000 3800 50  0001 C CNN
F 4 "YES" H 4250 3550 50  0001 C CNN "Place"
F 5 "NO" H 4250 3550 50  0001 C CNN "Provided"
F 6 "754-PAT1220C-6DBT5" H 4250 3550 50  0001 C CNN "Mouser"
	1    4250 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 3450 3850 3450
Text Label 3800 3450 2    50   ~ 0
AR60A_OUT
$Comp
L power:GND #PWR?
U 1 1 61B3D6CA
P 3100 3650
AR Path="/61B3D6CA" Ref="#PWR?"  Part="1" 
AR Path="/60F7742E/61B3D6CA" Ref="#PWR?"  Part="1" 
AR Path="/60F7742E/61AE0437/61B3D6CA" Ref="#PWR01402"  Part="1" 
F 0 "#PWR01402" H 3100 3400 50  0001 C CNN
F 1 "GND" H 3105 3477 50  0000 C CNN
F 2 "" H 3100 3650 50  0001 C CNN
F 3 "" H 3100 3650 50  0001 C CNN
	1    3100 3650
	-1   0    0    -1  
$EndComp
Text Label 5150 5850 2    50   ~ 0
AR60A_f_Adj
Wire Wire Line
	4500 5250 5150 5250
Wire Wire Line
	4500 5850 5150 5850
Wire Wire Line
	4500 5950 5150 5950
Wire Wire Line
	4500 6450 5150 6450
Text Label 3200 6850 0    50   ~ 0
AR60A_+15V
Text Label 5150 6450 2    50   ~ 0
AR60A_OUT_ATT
Text Label 5150 5950 2    50   ~ 0
AR60A_Vref_5V
Text Label 3200 5550 0    50   ~ 0
AR60A_RxD_RS232
Text Label 3200 5650 0    50   ~ 0
AR60A_TxD_RS232
Text Label 5150 5250 2    50   ~ 0
~AR60A_LOCK
Text Notes 3500 1700 0    50   ~ 0
ADCCUBEAT AR60A:\n1    +12 V or +15 V or +18 V - +36 V\n2    GND\n3    LOCK (BIT)\n4    Vref 5V (opt)\n5    GND\n6    TxD (opt)\n7    Factory Use\n8    Freq Adj (opt)\n9    RxD (opt)
Wire Wire Line
	4000 2750 4000 2950
Wire Wire Line
	4100 2750 4000 2750
Wire Wire Line
	3350 2650 4100 2650
Wire Wire Line
	4100 2550 3350 2550
Wire Wire Line
	4100 2450 3350 2450
Wire Wire Line
	4100 2350 3350 2350
Wire Wire Line
	4100 2050 3350 2050
Wire Wire Line
	4100 1950 3350 1950
Text Label 3350 2550 0    50   ~ 0
AR60A_Vref_5V
Text Label 3350 2450 0    50   ~ 0
AR60A_f_Adj
Text Label 3350 2650 0    50   ~ 0
AR60A_RxD_RS232
Text Label 3350 2050 0    50   ~ 0
AR60A_TxD_RS232
Text Label 3350 2350 0    50   ~ 0
~AR60A_LOCK
Text Label 3350 1950 0    50   ~ 0
AR60A_+15V
NoConn ~ 4100 2250
Connection ~ 4000 2750
Wire Wire Line
	4000 2150 4000 2750
Wire Wire Line
	4100 2150 4000 2150
$Comp
L power:GND #PWR?
U 1 1 61B3D687
P 4000 2950
AR Path="/61B3D687" Ref="#PWR?"  Part="1" 
AR Path="/60F7742E/61B3D687" Ref="#PWR?"  Part="1" 
AR Path="/60F7742E/61AE0437/61B3D687" Ref="#PWR01404"  Part="1" 
F 0 "#PWR01404" H 4000 2700 50  0001 C CNN
F 1 "GND" H 4005 2777 50  0000 C CNN
F 2 "" H 4000 2950 50  0001 C CNN
F 3 "" H 4000 2950 50  0001 C CNN
	1    4000 2950
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR?
U 1 1 61B3D681
P 4400 2950
AR Path="/61B3D681" Ref="#PWR?"  Part="1" 
AR Path="/60F7742E/61B3D681" Ref="#PWR?"  Part="1" 
AR Path="/60F7742E/61AE0437/61B3D681" Ref="#PWR01406"  Part="1" 
F 0 "#PWR01406" H 4400 2700 50  0001 C CNN
F 1 "Earth" H 4400 2800 50  0001 C CNN
F 2 "" H 4400 2950 50  0001 C CNN
F 3 "~" H 4400 2950 50  0001 C CNN
	1    4400 2950
	1    0    0    -1  
$EndComp
$Comp
L Connector:DB9_Female_MountingHoles J?
U 1 1 61B3D67B
P 4400 2350
AR Path="/61B3D67B" Ref="J?"  Part="1" 
AR Path="/60F7742E/61B3D67B" Ref="J?"  Part="1" 
AR Path="/60F7742E/61AE0437/61B3D67B" Ref="J1403"  Part="1" 
F 0 "J1403" H 4580 2352 50  0000 L CNN
F 1 "DB9_Female_MountingHoles" H 4580 2261 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-9_Female_Vertical_P2.77x2.84mm_MountingHoles" H 4400 2350 50  0001 C CNN
F 3 " ~" H 4400 2350 50  0001 C CNN
F 4 "NO" H 4400 2350 50  0001 C CNN "Place"
F 5 "NO" H 4400 2350 50  0001 C CNN "Provided"
	1    4400 2350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
